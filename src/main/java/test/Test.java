package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.*;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class Test {
	public static int nFeatures = 3;
	public static int nEpochs = 300000;
	public static double trainportion = 0.7;

	public static void main(String[] args) {
		run();
	}
	
	public static void run() {
		//create random data
		DataSet trainingdata = createTrainingData();
		System.out.println(trainingdata.numExamples() + "\t" + trainingdata.numInputs() + "\t" + trainingdata.numOutcomes());
		//split the training data
		SplitTestAndTrain split = trainingdata.splitTestAndTrain(trainportion);
		DataSet train = split.getTrain();
		DataSet test = split.getTest();
		//initialize network
		MultiLayerNetwork model = initNetwork();
		//fit the network
		model = fit(model, train);
		//test the network
		testNetwork(model, test);
	}
	
	/**
	 * Create some random dataset
	 */
	public static DataSet createTrainingData() {
		int nSamples = 1000;
		
		
		//Create features
		INDArray features = Nd4j.rand(new int[]{nSamples, nFeatures});
		List<DataSet> dataList = new ArrayList<>();
		for(int i = 0; i < features.rows(); i++) {
			 //Create label
			 double sum = 0;
			 INDArray row = features.getRow(i);
			 for(int k = 0; k < row.columns(); k++) {
				 sum += (k+1) * row.getDouble(k);
			 }
			 INDArray label = Nd4j.create(new double[] {sum}, new int[] {1});
			 //Create dataset object and add it to the list
			 dataList.add(new DataSet(row, label));
		}
		Collections.shuffle(dataList);
		return DataSet.merge(dataList);
	}
	
	/**
	 * Create Neural Network
	 */
	public static MultiLayerNetwork initNetwork() {
		int seed = 1;
		int iterations = 1;
		int neurons_hidden = 10;
		boolean regularization = true;
		double learningrate = 1e-3;
		
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
				.seed(seed)
				.iterations(iterations)
				.regularization(regularization)//.l1(l1).l2(l2)
				.learningRate(learningrate)
				.activation(Activation.SIGMOID)
				.weightInit(WeightInit.XAVIER)
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
				.updater(Updater.ADAM)
				.list()
			.layer(0, new DenseLayer.Builder()
						.nIn(nFeatures)
						.nOut(neurons_hidden)
						.activation(Activation.IDENTITY)
						.build())
			.layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
					.nOut(1)
					.activation(Activation.IDENTITY)
					.build())
			.backprop(true).pretrain(true)
			.build();
		
		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		
		model.setListeners(new ScoreIterationListener(10));
		
		return model;
	}
	
	/**
	 * Fit the model on the specified dataset
	 * @param model
	 * @param data
	 */
	public static MultiLayerNetwork fit(MultiLayerNetwork model, DataSet data) {
		for(int i = 0; i < nEpochs; i++) {
			model.fit(data);
		}
		return model;
	}
	
	/**
	 * Test the model on the specified dataset
	 * @param model
	 * @param data
	 */
	public static void testNetwork(MultiLayerNetwork model, DataSet data) {
		INDArray labels = data.getLabels();
		INDArray prediction = model.output(data.getFeatureMatrix(), false);
		RegressionEvaluation eval = new RegressionEvaluation(1);
		eval.eval(labels, prediction);
		for(int i = 0; i < 20; i++) {
			System.out.println("Features:" + data.getFeatures().getRow(i) + "; Label:" + labels.getDouble(i) + "; Prediction:" + prediction.getDouble(i));
		}
		System.out.println(eval.stats());
	}

}
